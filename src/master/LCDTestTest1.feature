
Feature:AppFcnSelBroswer
  <Some interesting description here>
  the application must function correctly in Internet Explorer 6 and 7, Firefox 1.5 and 2, and Safari 2 so that as many people as possible can fully use the application.

@LcdTestAccessIE6 
@LcdTestAccessIE7
@LcdTestAccessFF1_5 
@LcdTestAccessFF2
@LcdTestAccessSafari2

Scenario Outline: TestAppFcnBrowser
    Given web application is accessible 
    When https://www.webApp.com is entered in "<webBrowser>"
    Then the application login page appears
    
Examples:

|webBrowser|
|Internet Explorer 6| 
|Internet Explorer 7|
| Firefox 1.5       |
| Firefox 2         |
| Safari 2          |